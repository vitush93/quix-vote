const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('people.db');
const json2csv = require('json2csv');
const fs = require('fs');
const validators = require('./src/validators');

// setup local database
db.serialize(function () {
    db.run("CREATE TABLE IF NOT EXISTS votes(id INTEGER PRIMARY KEY AUTOINCREMENT, name text NOT NULL, number text NOT NULL)");
});

// init express app
const app = express();

// configure middleware
app.use(cors({
    origin: '*'
}));
app.use(bodyParser.json());


// job sync flag
let needRefresh = true;

// update file.csv every second, if needed
setInterval(function () {

    if (!needRefresh) return;

    const wstream = fs.createWriteStream('build/file.txt');
    db.each("SELECT name, number FROM votes", function (err, row) {
        wstream.write(row.name + "\t" + row.number + "\n")

    }, function () {
        wstream.end();
        console.log('file refreshed!');

        needRefresh = false;
    });
}, 1000);


// writes a new record to the database
app.post('/', function (req, res) {
    const name = req.body.name;
    const number = req.body.number;

    if (!validators.name(name) || !validators.number(number)) {
        res.json({
            success: false
        });

        return;
    }

    db.run("INSERT INTO votes(name, number) VALUES(?, ?)", name, number, function (err) {
        if(err) console.log(err); else console.log('record inserted!');

        needRefresh = true;
    });

    res.json({
        success: true
    });
});


app.listen(8080, function () {
    console.log('Example app listening on port 8080!');
});