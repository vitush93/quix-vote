import React from 'react';
import {Header} from "semantic-ui-react";
import {withRouter} from "react-router-dom";
import Lockr from 'lockr';


const Success = ({history}) => {

    Lockr.set('name', '');
    Lockr.set('number', '');

    setTimeout(function () {
        history.push('/');
    }, 4000);

    return (
        <div>
            <br/>
            <br/>
            <br/>
            <Header as='h2' style={{textAlign: 'center', color: "#970f69"}}>Thank you for your kind donation!</Header>
        </div>
    );
};

export default withRouter(Success);