import React from 'react';
import { withRouter } from "react-router-dom";
import { Form } from "semantic-ui-react";
import Lockr from 'lockr';
import validators from './validators';

class Home extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name: Lockr.get('name', ''),
            number: Lockr.get('number', ''),
            nameError: false,
            numberError: false
        };
    }

    onNameChange(e) {
        const value = e.target.value;

        this.setState({ name: value });
    }

    onNumberChange(e) {
        const value = e.target.value;

        this.setState({ number: value });
    }

    onFormSubmit(e) {
        e.preventDefault();

        // clean whitespace from values
        const name = this.state.name.trim();
        const number = this.state.number;

        // validate inputs
        const nameError = !validators.name(name);
        const numberError = !validators.number(number);

        this.setState({ nameError, numberError });

        if (nameError || numberError) return;

        // store form data in local storage
        Lockr.set('name', name);
        Lockr.set('number', number);

        // open Check component
        this.props.history.push('/check');
    }

    onBidChange(e, { value }) {
        this.setState({ number: value });
    }

    render() {
        return (
            <div>
                <br />
                <br />
                <br />
                <Form size="big">
                    <Form.Input label="Your ID" value={this.state.name} onChange={this.onNameChange.bind(this)}
                        error={this.state.nameError}
                        placeholder="John Doe" required />

                    <br />
                    <Form.Field required>
                        <label style={this.state.numberError ? {color: "#9f3a38"} : {}}>Choose the amount you would like to donate:</label>
                    </Form.Field>

                        <table id="form-table" style={{margin: 'auto', width: '100%'}}>
                            <tr>
                                <td><Form.Radio label="5 000,-" value="5000" onChange={this.onBidChange.bind(this)} checked={this.state.number == 5000} /></td>
                                <td><Form.Radio label="50 000,-" value="50000" onChange={this.onBidChange.bind(this)} checked={this.state.number == 50000} /></td>
                            </tr>



                            <tr>
                                <td><Form.Radio label="10 000,-" value="10000" onChange={this.onBidChange.bind(this)} checked={this.state.number == 10000} /></td>
                                <td><Form.Radio label="100 000,-" value="100000" onChange={this.onBidChange.bind(this)} checked={this.state.number == 100000} /></td>
                            </tr>



                            <tr>
                                <td><Form.Radio label="25 000,-" value="25000" onChange={this.onBidChange.bind(this)} checked={this.state.number == 25000} /></td>
                                <td><Form.Radio label="250 000,-" value="250000" onChange={this.onBidChange.bind(this)} checked={this.state.number == 250000} /></td>
                            </tr>
                        </table>


                    <br />
                    <Form.Button color="blue" fluid onClick={this.onFormSubmit.bind(this)}>Proceed</Form.Button>
                </Form>
            </div>
        );
    }
}

export default withRouter(Home);