import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";
import Home from "./Home";
import Success from "./Success";
import {Container, Grid} from "semantic-ui-react";
import Check from "./Check";
import Fail from "./Fail";


const App = () => {
    return (
        <Container>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <Grid columns={16} stackable>
                <Grid.Row>
                    <Grid.Column />
                    <Grid.Column width={14}>
                        <Switch>
                            <Route exact path="/" component={Home}/>
                            <Route path="/success" component={Success}/>
                            <Route path="/fail" component={Fail}/>
                            <Route path="/check" component={Check}/>
                        </Switch>
                    </Grid.Column>
                    <Grid.Column />
                </Grid.Row>
            </Grid>
        </Container>
    );
};

export default App;
