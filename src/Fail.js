import React from 'react';
import {withRouter} from "react-router-dom";
import {Header} from "semantic-ui-react";

const Fail = ({history}) => {

    setTimeout(function () {
        history.push('/');
    }, 4000);

    return (
        <div>
            <br/>
            <br/>
            <br/>
            <Header as='h2'>Sorry, something went wrong.</Header>
            <p>Please try again later.</p>
            <p>You will be redirected back automatically..</p>
        </div>
    );
};

export default withRouter(Fail);