var validators = {
    name: (name) => {
        return (name.length > 0)
    },

    number: (number) => {
        // return (number.length > 0) && /^[\d|.|,]+$/.test(number);
        return number.toString().length > 0;
    }
};

module.exports = validators;