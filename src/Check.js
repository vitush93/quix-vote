import React from 'react';
import {Button, Form, Header, Table} from "semantic-ui-react";
import {withRouter} from "react-router-dom";
import axios from 'axios';
import Lockr from 'lockr';
import config from './config';

const Check = ({history}) => {

    const name = Lockr.get('name', '');
    const number = Lockr.get('number', '');

    const okClicked = (e) => {
        e.preventDefault();

        // submit data to server
        axios.post(config.serverUrl, {name, number})
            .then(res => {
                Lockr.set('number', ''); // remove number from local storage

                if (!res.data.success) {
                    history.push('/fail');
                } else {
                    history.push('/success')
                }
            })
            .catch(err => {
                console.log(err);
            });
    };

    const backClicked = (e) => {
        e.preventDefault();

        history.push('/');
    };

    return (
        <div>
            <br/>
            <br/>
            <br/>
            <Header as='h2'>Would you like to donate this amount?</Header>
            <br/>
            <Table basic="very" celled padded unstackable>
                <Table.Body>
                    <Table.Row>
                        <Table.Cell textAlign="right" style={{borderBottom: "1px solid #970f69"}}>
                            <Header as="h4">ID:</Header>
                        </Table.Cell>
                        <Table.Cell style={{borderLeft: "1px solid #970f69", borderBottom: "1px solid #970f69"}}>
                            {name}
                        </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell textAlign="right">
                            <Header as="h4">Amount:</Header>
                        </Table.Cell>
                        <Table.Cell style={{borderLeft: "1px solid #970f69"}}>
                            {number},- Kč
                        </Table.Cell>
                    </Table.Row>
                </Table.Body>
            </Table>
            <br/>

            <Form>
                <Form.Group widths="equal">
                    <Button fluid color="green" onClick={okClicked}>OK</Button><br />
                    <Button fluid onClick={backClicked}>Go back</Button>
                </Form.Group>
            </Form>
        </div>
    )
};

export default withRouter(Check);